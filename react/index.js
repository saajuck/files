/*
 *
 * Club
 *
 */
import SubNav from 'components/SubNav'
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Helmet } from 'react-helmet'
import conf from 'utils/env'
import { loadClubDetail, setClubId, setCompetitionId, loadClub } from './actions'
import General from './General'

import { makeSelectClub, makeSelectLoading, makeSelectError, makeSelectMatches } from './selectors'

export class Club extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props)
    this.props.setClubId(this.props.params.slug)
    this.props.setCompetitionId(this.props.params.idCompetition)
  }

  componentDidMount() {
    if (!this.props.clubs.getIn([this.props.params.slug, 'detail'])) this.props.loadClubDetail()
    this.props.loadClub()
  }

  render() {
    const clubDetail = this.props.clubs.getIn([this.props.params.slug, 'detail']) || null
    const pickedCompetition = this.props.clubs.getIn([this.props.params.slug, 'competitionSelected']) || null

    const Child = this.props.children ? this.props.children : <General {...this.props.matches} clubDetail={clubDetail} pickedCompetition={pickedCompetition} />
    return (
      <div id="club-information">
        <Helmet>
          <title>{clubDetail.name}</title>
        </Helmet>

        <header className="club-header">
          {clubDetail ? (
            <div className="club-header-content">
              {clubDetail.logo ?
                (<img src={`${conf.requestURL}/logo/${clubDetail.id}.jpeg`} alt={clubDetail.name} />)
                : (<img src="http://placehold.it/50x50" alt="logo club" />)
              }
              <h1>{clubDetail.name}</h1>
              {pickedCompetition ? (
                <span>{pickedCompetition.name}</span>
              ) : null}
              <a href="" alt="">Suivre</a>
            </div>
          ) : null}
        </header>

        <SubNav slug={this.props.params.slug} idCompetition={this.props.params.idCompetition} />

        <main>
          {Child}
        </main>
      </div>

    )
  }
}

Club.propTypes = {
  matches: PropTypes.object,
  clubs: PropTypes.object,
  loadClub: PropTypes.func,
  loadClubDetail: PropTypes.func,
  setCompetitionId: PropTypes.func,
  params: PropTypes.object,
  setClubId: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
}

const mapStateToProps = createStructuredSelector({
  clubs: makeSelectClub(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  matches: makeSelectMatches(),
})

function mapDispatchToProps(dispatch) {
  return {
    loadClub: () => { dispatch(loadClub()) },
    loadClubDetail: () => { dispatch(loadClubDetail()) },
    setClubId: (id) => { dispatch(setClubId(id)) },
    setCompetitionId: (id) => { dispatch(setCompetitionId(id)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Club)
