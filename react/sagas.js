import { take, call, put, select, cancel, takeLatest } from 'redux-saga/effects'
import { LOCATION_CHANGE } from 'react-router-redux'
import request from 'utils/request'
import conf from 'utils/env'
import { LOAD_CLUB, LOAD_CLUB_DETAIL } from './constants'
import { loadClubSuccess, loadClubError, loadClubDetailSuccess, storeMatches } from './actions'
import { makeSelectClubId, makeSelectCompetitionId } from './selectors'

export function* getClub() {
  const slug = yield select(makeSelectClubId())
  const idCompetition = yield select(makeSelectCompetitionId())
  try {
    const club = yield call(request, `${conf.requestURL}/${conf.apiVersion}/club/${slug}/competition/${idCompetition}`)
    club.slug = slug
    yield put(loadClubSuccess(club))
    yield put(storeMatches(filterMatches(club)))
  } catch (error) {
    yield put(loadClubError(error))
  }
}

export function* getClubDetail() {
  const slug = yield select(makeSelectClubId())
  try {
    const club = yield call(request, `${conf.requestURL}/${conf.apiVersion}/club/${slug}`)
    yield put(loadClubDetailSuccess(club))
  } catch (error) {
    yield put(loadClubError(error))
  }
}

export function* clubData() {
  const loadClub = yield takeLatest(LOAD_CLUB, getClub)
  const loadClubDetail = yield takeLatest(LOAD_CLUB_DETAIL, getClubDetail)

  yield take(LOCATION_CHANGE)

  yield cancel(loadClub)
  yield cancel(loadClubDetail)
}

export default [
  clubData,
]

function filterMatches(club) {
  const now = new Date().getTime()
  let allMatches = []
  club.competition_phases.map((phase) =>
    phase.competition_jours.find((curr) => {
      allMatches = allMatches.concat(curr.matches)
      return new Date(curr.date).getTime() > now
    })
  )
  const nextMatch = allMatches.reduce((prev, cur) => {
    if (!prev) return cur
    if (new Date(cur.date).getTime() - now < 0) return prev
    return ((Math.abs(new Date(cur.date).getTime() - now) < Math.abs(new Date(prev.date).getTime() - now)) ? cur : prev)
  }, null)

  const prevMatch = allMatches.reduce((prev, cur) => {
    if (!prev) return cur
    if (new Date(cur.date).getTime() - now > 0) return prev
    return ((Math.abs(new Date(cur.date).getTime() - now) < Math.abs(new Date(prev.date).getTime() - now)) ? cur : prev)
  }, null)

  return {
    nextMatch,
    prevMatch,
  }
}
