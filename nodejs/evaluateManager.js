
var actionDispatcher = function (action) {
    var evaluateManager = {
        look_for_title: function () {
            var titles_tag = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
            var titles = [];
            for (var i = 0; i < titles_tag.length; i++) {
                var titles_dom = document.getElementsByTagName(titles_tag[i])
                if (titles_dom !== undefined) {
                    for (var j = 0; j < titles_dom.length; j++) {
                        var current_obj = {
                            'DOM': titles_tag[i],
                            'innerText': titles_dom[j].innerText
                        }
                        titles.push(current_obj);
                    }
                }
            }
            return titles;
        },

        look_for_iframe: function () {
            var iframes = [];
            var dom = document.getElementsByTagName('iframe');
            for (var i = 0; i < dom.length; i++) {
                if (dom[i].src.length)
                    iframes.push(dom[i].src);
            }
            return iframes
        },

        look_for_link: function () {
            var a_dom = document.getElementsByTagName('a');
            var a = [];
            for (var i = 0; i < a_dom.length; i++) {
                var cur_a = {}
                for (var j = 0; j < a_dom[i].attributes.length; j++) {
                    cur_a[a_dom[i].attributes[j].name] = a_dom[i].attributes[j].value;
                }
                a.push(cur_a);
            }
            return a;
        },

        look_for_query: function (target) {
            var e_dom = document.querySelectorAll(target);
            if (!e_dom)
                return null;

            var e = [];
            for (var i = 0; i < e_dom.length; i++) {
                var cur_e = {}
                for (var j = 0; j < e_dom[i].attributes.length; j++) {
                    cur_e[e_dom[i].attributes[j].name] = e_dom[i].attributes[j].value;
                }
                e.push(cur_e);
            }
            return e;
        }
    }


    var data = {};  
    var test =""
    switch (action.type) {
        case 'look_for_link':
            if((test = evaluateManager.look_for_link()) )
              data.link = test;
            break;

        case 'look_for_title':
            if((test = evaluateManager.look_for_title() ))
            data.hierarchie = test
            break;

        case 'look_for_iframe':
            if((test = evaluateManager.look_for_iframe()))
                data.iframe = test
            break; 

        case 'look_for_query':
                
                if(test = evaluateManager.look_for_query(action.param)){
                    data.customQuery = {
                        param: action.param,
                        data : evaluateManager.look_for_query(action.param)
                    }
                }
              
            break; 
 
        default:
            console.log('invalid action called');
    }
    return data
    
}

if(typeof module !== 'undefined')
    module.exports = actionDispatcher;
