const fs = require("fs");
const actionDispatcher = require("./evaluateManager");
const colors = require("colors");
const URI = require("urijs");
const myEmitter = require("./event");

const spider = function(config, client_redis) {
  const instance = this;
  client_redis
    .multi()
    .lpop("pending_url")
    .llen("pending_url")
    .llen("visited_url")
    .lrange("pending_url", 0, -1)
    .lrange("visited_url", 0, -1)
    .incr("page_id")
    .exec((er, res) => {
      const page = {
        url: res[0] || config.startUrl,
        pending_ll: res[1],
        visited_ll: res[2],
        pending: res[3],
        visited: res[4],
        id: res[5],
      };
      const url = res[0] || config.startUrl;
      client_redis.rpush("visited_url", page.url);
      spider_run(instance, config, client_redis, page);
    });
};

const spider_run = (instance, config, client_redis, page) => {
  const url = page.url;
  const configs = [actionDispatcher.toString(), config];

  instance.on("resourceError", resourceError => {
    console.log(
      `Unable to load resource (# ${resourceError.id} URL : ${resourceError.url}), code : ${resourceError.errorCode}`
    );
  });

  instance
    .open(url)
    .status()
    .then(e => {
      console.log("visited url : ".green + page.id);
      console.log(
        `status : ${e.toString()} for ${url} with horseman id ${instance.id.toString()}`.cyan
      );
      console.log(`remaining pending atm :${page.pending_ll}`);
      return this;
    })
    .injectJs("./modules/evaluateManager.js")
    .injectJs("./node_modules/urijs/src/URI.min.js")
    .evaluate(function evaluateConf(configs) {
      var config = configs[1];
      eval("var actionDispatcher = " + configs[0]);
      var eval_result = [];
      var l = config.evalutate.length;
      for (var i = 0; i < l; i++) {
        eval_result.push(actionDispatcher(config.evalutate[i]));
      }
      var links = [];
      Array.prototype.forEach.call(document.querySelectorAll("a"), e => {
        var href = e.getAttribute("href");
        if (href) {
          var a = URI(href).fragment("");
          if (
            a.is("url") &&
            links.indexOf(a.absoluteTo(config.startUrl).toString()) == -1 &&
            (config.startUrl.indexOf(a.hostname()) >= 0 ||
              a.hostname() == "") &&
            (a.subdomain() == "www" || a.subdomain() == "") &&
            a.filename().match(/\.(pdf|mp3|jpg|jpeg|etc)$/i) == null
          ) {
            links.push(a.absoluteTo(config.startUrl).toString());
          }
        }
      });
      var title = $("title").html();
      eval_result.push({ title: title });
      data = {
        links: links,
        eval_result: eval_result
      };
      return data;
    }, configs)
    .catch(err => {
      console.log("err catched");
      console.log(err);
      myEmitter.emit("close_this_one_pliz", instance);
    })
    .then(data => {
      if (data.links) {
        const links = data.links || "";
      }
      const multi = client_redis.multi();
      data.eval_result.push({ url: url });
      multi.rpush("page_" + page.id, JSON.stringify(data.eval_result));
      var count = 0;
      if (links) {
        ll = links.length;
        for (let i = 0; i < ll; i++) {
          const L = URI(links[i]);
          let exlude_this = false;
          if (config.exclude) {
            config.exclude.forEach(e => {
              if (e.param) {
                if (L[e.type](e.param)) {
                  exlude_this = true;
                }
              } else if (e.not) {
                if (L[e.type]() === e.not) {
                  exlude_this = true;
                }
              }
            });
          }
          if (
            page.pending.indexOf(links[i]) == -1 &&
            page.visited.indexOf(links[i]) == -1 &&
            !exlude_this
          ) {
            multi.rpush("pending_url", links[i]);
            count++;
          }
        }
        fs.mkdir("./screens/", (err, res) => {
          fs.writeFile(
            "./screens/page_" + page.id + ".json",
            JSON.stringify(data.eval_result),
            (er, re) => {
              console.log(er);
            }
          );
        });
      }
      multi.exec((err, rs) => {
        instance.screenshot("./screens/page_" + page.id + ".png");
        client_redis.llen("pending_url", (err, res) => {
          if (res > 0) {
            myEmitter.emit("new_instance_please");
            instance.spider(config, client_redis);
          } else {
            client_redis.lrange("visited_url", 0, -1, (err, reply) => {
              console.log("reply");
              console.log(reply);
              fs.writeFile(
                "./all_urls.json",
                JSON.stringify(reply),
                (er, re) => {}
              );
            });
            instance.close();
          }
        });
      });
    });
};

module.exports = spider;
